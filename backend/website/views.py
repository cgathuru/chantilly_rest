from django.shortcuts import render
from website.models import Article
from rest_framework import viewsets, serializers

# Create your views here.
class ArticleSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Article
        fields = '__all__'
        extra_kwargs = {
            'url': {'lookup_field': 'name'}
        }
        

class ArticleViewSet(viewsets.ModelViewSet):

    queryset = Article.objects.all()
    serializer_class = ArticleSerializer
    lookup_field = 'name'


